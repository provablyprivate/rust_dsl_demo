use pest::iterators::Pair;
use ppc::{
    abstract_syntax::abstract_syntax::*,
    identity::Identity,
    Type,
};

use crate::ppc_parser::Rule;

/// Interpret a 'file', which is the 'Pair' given as output of the parser. Each
/// 'line' of the file can be any of the rule supplied in our syntax, but only
/// the ones listed below should be possible, the typechecker will guarantee it
/// once implemented.
pub fn interpret_file(file: Pair<Rule>) -> ASPPC {
    let mut asppc = ASPPC::new();
    for next in file.into_inner() {
        match next.as_rule() {
            Rule::items => {
                for item in next.into_inner() {
                    let item = interpret_item(item);
                    asppc.add_item(item);
                }
            }
            Rule::agents => {
                for agent in next.into_inner() {
                    let agent = interpret_agent(agent);
                    asppc.add_agent(agent)
                }
            }
            Rule::negative_constraints => {
                for negative_constraint in next.into_inner() {
                    let negative_constraint =
                        interpret_negative_constraint(negative_constraint);
                    asppc.add_negative_constraint(negative_constraint)
                }
            }
            Rule::positive_constraints => {
                for positive_constraint in next.into_inner() {
                    let positive_constraint =
                        interpret_positive_constraint(positive_constraint);
                    asppc.add_positive_constraint(positive_constraint)
                }
            }
            Rule::constructions => {
                for construct in next.into_inner() {
                    let construct = interpret_construction(construct);
                    asppc.add_construction(construct)
                }
            }
            Rule::EOI => {}
            _ => {
                panic!(
                    "Line not recognized as possible base: {:?}",
                    next.as_rule()
                );
            }
        }
    }
    asppc
}

/// Base token, always converts into a string. This is used for IDs of agents
/// and items.
fn interpret_unique_identifier(unique_identifier: Pair<Rule>) -> Identity {
    unique_identifier.as_str().to_string()
}

/// For items which are part of constructions or messages, returns an
/// architecturally compatible content.
fn interpret_item(item: Pair<Rule>) -> ASItem {
    Type::atomic(&interpret_unique_identifier(item))
}

/// For single line agents, this method constructs the state of all the agents
/// declared at the start of the input file.
fn interpret_agent(agent: Pair<Rule>) -> ASAgent {
    interpret_unique_identifier(agent)
}

fn interpret_negative_constraint(
    negative_constraint: Pair<Rule>,
) -> ASNegativeConstraint {
    let mut inner = negative_constraint.into_inner();
    let mut current = inner.peek();
    let agent1: Identity;
    let item1: Type;
    let agent2: Identity;
    let item2: Type;
    match inner.next().unwrap().as_rule() {
        Rule::agent => {
            agent1 = interpret_unique_identifier(
                current.unwrap().into_inner().peek().unwrap(),
            );
        }
        _ => {
            panic!("Expected agent, found: {:?}", current)
        }
    }
    current = inner.peek();
    match inner.next().unwrap().as_rule() {
        Rule::item => {
            item1 = interpret_item(current.unwrap());
        }
        _ => {
            panic!("Expected item, found: {:?}", current)
        }
    }
    current = inner.peek();
    match inner.next().unwrap().as_rule() {
        Rule::agent => {
            agent2 = interpret_unique_identifier(
                current.unwrap().into_inner().peek().unwrap(),
            );
        }
        _ => {
            panic!("Expected agent, found: {:?}", current)
        }
    }
    current = inner.peek();
    match inner.next().unwrap().as_rule() {
        Rule::item => {
            item2 = interpret_item(current.unwrap());
        }
        _ => {
            panic!("Expected item, found: {:?}", current)
        }
    }
    (agent1, item1, agent2, item2)
}

fn interpret_positive_constraint(
    positive_constraint: Pair<Rule>,
) -> ASPositiveConstraint {
    let mut inner = positive_constraint.into_inner();
    let mut current = inner.peek();
    let agent: Identity;
    let proditem: Type;
    match inner.next().unwrap().as_rule() {
        Rule::agent => {
            agent = interpret_unique_identifier(
                current.unwrap().into_inner().peek().unwrap(),
            );
        }
        _ => {
            panic!("Expected AGENT, found {:?}.", current.unwrap().as_rule());
        }
    }
    current = inner.peek();
    match inner.next().unwrap().as_rule() {
        Rule::item => {
            proditem = interpret_item(current.unwrap());
        }
        _ => {
            panic!("Expected ITEM, found {:?}.", current.unwrap().as_rule());
        }
    }
    (agent, proditem)
}

/// Returns a constructor defined as a construction in the input file to the
/// relevant agent. Typechecker will in this case already have verified that the
/// agent and the items are valid.
fn interpret_construction(construction: Pair<Rule>) -> ASConstruction {
    let mut inner = construction.into_inner();
    let mut current = inner.peek();
    let agent: Identity;
    let proditem: Type;
    match inner.next().unwrap().as_rule() {
        Rule::agent => {
            agent = interpret_unique_identifier(
                current.unwrap().into_inner().peek().unwrap(),
            );
        }
        _ => {
            panic!("Expected AGENT, found {:?}.", current.unwrap().as_rule());
        }
    }
    current = inner.peek();
    match inner.next().unwrap().as_rule() {
        Rule::item => {
            proditem = interpret_item(current.unwrap());
        }
        _ => {
            panic!("Expected ITEM, found {:?}.", current.unwrap().as_rule());
        }
    }
    let mut use_items: Vec<Type> = Vec::new();
    for next in inner {
        match next.as_rule() {
            Rule::item => {
                use_items.push(interpret_item(next));
            }
            _ => {
                panic!("Expected AITEM, found {:?}.", next.as_rule());
            }
        }
    }
    (agent, proditem, use_items)
}

#[cfg(test)]
mod tests {
    use std::fs;

    use super::*;
    use crate::ppc_parser;

    // Small test to check if parsing the demo input is successful. Does not
    // check the actual result of the parsing
    #[test]
    fn test_interpreting() {
        let file =
            fs::read_to_string("src/DemoInput.txt").expect("cannot read file");
        let parsed_file = ppc_parser::ppc_parse(&file);
        let asppc = interpret_file(parsed_file);

        let mut expected_asppc = ASPPC::new();

        expected_asppc.add_item(Type::atomic("Info"));
        expected_asppc.add_item(Type::atomic("Consent"));
        expected_asppc.add_item(Type::atomic("Policy"));

        expected_asppc.add_agent(String::from("Child"));
        expected_asppc.add_agent(String::from("Website"));
        expected_asppc.add_agent(String::from("Parent"));

        expected_asppc.add_construction((
            String::from("Child"),
            Type::atomic("Info"),
            vec![],
        ));
        expected_asppc.add_construction((
            String::from("Parent"),
            Type::atomic("Consent"),
            vec![],
        ));
        expected_asppc.add_construction((
            String::from("Website"),
            Type::atomic("Policy"),
            vec![],
        ));

        expected_asppc.add_negative_constraint((
            String::from("Website"),
            Type::atomic("Info"),
            String::from("Website"),
            Type::atomic("Consent"),
        ));
        expected_asppc.add_negative_constraint((
            String::from("Website"),
            Type::atomic("Consent"),
            String::from("Parent"),
            Type::atomic("Policy"),
        ));

        expected_asppc.add_positive_constraint((
            String::from("Website"),
            Type::atomic("Consent"),
        ));
        expected_asppc.add_positive_constraint((
            String::from("Website"),
            Type::atomic("Info"),
        ));
        expected_asppc.add_positive_constraint((
            String::from("Parent"),
            Type::atomic("Policy"),
        ));

        assert_eq!(expected_asppc, asppc);
    }
}
