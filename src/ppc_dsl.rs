use ppc::abstract_syntax::abstract_syntax::ASPPC;

use crate::ppc_interpreter::interpret_file;
use crate::ppc_parser::ppc_parse;

/// Parses, typechecks(eventually) and interprets the input into an
/// architecture.
pub fn get_as(input: String) -> ASPPC {
    let parsed_input = ppc_parse(&input);
    interpret_file(parsed_input)
}
